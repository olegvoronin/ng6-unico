import template from './shoppingCart.html';
import controller from './shoppingCart.controller';

let shoppingCartComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default shoppingCartComponent;
