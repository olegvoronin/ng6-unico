import angular from 'angular';
import uiRouter from 'angular-ui-router';
import shoppingCartComponent from './shoppingCart.component';

let shoppingCartModule = angular.module('shoppingCart', [
  uiRouter
])

.config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('cart', {
      url: '/cart',
      component: 'shoppingCart'
    });
})

.component('shoppingCart', shoppingCartComponent)

.name;

export default shoppingCartModule;
