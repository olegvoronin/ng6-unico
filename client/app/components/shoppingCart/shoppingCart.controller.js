class ShoppingCartController {
  constructor(Cart) {
    this.name = 'shoppingCart';
    this.cartItems = Cart;
  }
}

ShoppingCartController.$inject = ["Cart"];

export default ShoppingCartController;
