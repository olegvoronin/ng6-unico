import payload from './recommendedProducts.payload.json';

class RecommendedProductsController {
  constructor(Cart) {
    this.name = 'recommendedProducts';
    this.cartItems = Cart;
    this.products = payload.products;
  }
}

RecommendedProductsController.$inject = ["Cart"];

export default RecommendedProductsController;
