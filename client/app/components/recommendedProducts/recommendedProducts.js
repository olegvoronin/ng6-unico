import angular from 'angular';
import uiRouter from 'angular-ui-router';
import recommendedProductsComponent from './recommendedProducts.component';

let recommendedProductsModule = angular.module('recommendedProducts', [
  uiRouter
])

.config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('recommendedProducts', {
      url: '/',
      component: 'recommendedProducts'
    });
})

.component('recommendedProducts', recommendedProductsComponent)

.name;

export default recommendedProductsModule;
