import template from './recommendedProducts.html';
import controller from './recommendedProducts.controller';

let recommendedProductsComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default recommendedProductsComponent;
