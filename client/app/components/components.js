import angular from 'angular';
import ShoppingCart from './shoppingCart/shoppingCart';
import RecommendedProducts from './recommendedProducts/recommendedProducts';

let componentModule = angular.module('app.components', [
  ShoppingCart,
  RecommendedProducts
])

.name;

export default componentModule;
