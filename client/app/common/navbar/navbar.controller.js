class NavbarController {
  constructor(Cart) {
    this.name = 'navbar';
    this.cartItems = Cart;
  }
}

NavbarController.$inject = ["Cart"];

export default NavbarController;
