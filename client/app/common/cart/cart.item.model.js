function CartItem(_item){
  var vm = this;
  vm.id = _item.id;
  vm.title = _item.title;
  vm.category = _item.category;
  vm.imageUrl = _item.imageUrl;
  vm.unitsInCartons = _item.unitsInCartons;
  vm.unitCost = _item.unitCost;
  vm.packSize = _item.packSize;
  vm.secondaryCategory = _item.secondaryCategory;
  // vm.qty = _item.qty;

  var _qty;
  Object.defineProperty(vm,'qty',{
    set: function(val){
      _qty = Math.max(Math.min(val, 100), 1);
      return _qty;
    },
    get: function(){
      return _qty;
    }
  });
  _qty = _item.qty * 1;

  vm.getPrice = function(){
    return vm.unitCost * vm.packSize * vm.qty;
  };
}

export default CartItem;