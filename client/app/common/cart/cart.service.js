import CartItem from './cart.item.model';

let CartService = function () {
  const cartItems = [];

  let addItem = (item) => {
    let existingItem = cartItems.find((_i) => {
      return _i.id === item.id;
    });
    if (existingItem) {
      existingItem.qty++;
    } else {
      let newItem = Object.assign({ qty: 1 }, item)
      // cartItems.push(new CartItem(item.id, item.unitCost, item.packSize, 1));
      cartItems.push(new CartItem(newItem));
    }
  };

  let removeItem = (item) => {
    let index = cartItems.indexOf(item);
    cartItems.splice(index, 1);
  };

  let totalProducts = () => {
    return cartItems.length;
  };

  let totalCost = () => {
    return cartItems.map(_i => {
      return _i.unitCost * _i.packSize * _i.qty;
    }).reduce((_prev, _cur) => {
      return _prev + _cur;
    }, 0);
  };

  return { cartItems, addItem, removeItem, totalProducts, totalCost };
};


export default CartService;