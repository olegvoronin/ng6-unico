import angular from 'angular';
import Cart from './cart.service';

let cartModule = angular.module('Cart', [])

.service('Cart', Cart)
  
.name;

export default cartModule;
